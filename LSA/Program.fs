﻿open System
open System.Linq
open System.Collections.Generic
open System.Windows.Forms
open MSDN.FSharp.Charting
open MathNet.Numerics.LinearAlgebra.Double


let t0 = "the neatest little guide to stock market investing"
let t1 = "investing for dummies, 4th edition"
let t2 = "the little book of common sense investing: the only way to guarantee your fair share of stock market returns"
let t3 = "the little book of value investing"
let t4 = "value investing: from graham to buffet and beyond"
let t5 = "rich dad's guide to investing: what the rich invest in, that the poor and the middle class do not!"
let t6 = "investing in real estate, 5th edition"
let t7 = "stock investing for dummies"
let t8 = "rich dad's advisors the abc's of real estate investing: the secrets of finding hidden profits most investors miss"

let ignoreList = [|"the"; "to"; "and"; "in"; "of"; "little"; "to"; "from"; "for"; "edition"|]

let docList = [|t0; t1; t2; t3; t4; t5; t6; t7; t8|]

let wordInIgnoreList list word =
    if Array.exists (fun e -> e = word) list then true
    else false

let buildMatrix (titles:string[]) =    
    let countLocalOccurrences (_word:string) (_doc:string[]) =
        Array.fold (fun acc elem -> if elem = _word then (acc + 1.0) else acc) 0.0 _doc
    
    let countGlobalOccurrences _word (_dict:SortedList<string, List<float>>) =
        Seq.fold (fun acc elem -> if elem > 0.0 then (acc + 1) else acc) 0 _dict.[_word]
       
    let updateDict (_dict:SortedList<string, List<float>>) _word _doc =
        if _dict.ContainsKey(_word) then 
            _dict.[_word].Add(countLocalOccurrences _word _doc)
        else
            _dict.Add(_word, new List<float>())
            _dict.[_word].Add(countLocalOccurrences _word _doc)
        
    let dict = new SortedList<string, List<float>>()    
    
    let wordBag = [|
        for title in titles do 
            yield title.ToLower().Split([|' '; ','; '.'; ':'|], StringSplitOptions.RemoveEmptyEntries)
            |> Array.filter (fun e -> not (wordInIgnoreList ignoreList e)) |]    
    
    let wordSet = new SortedSet<string>(Array.concat wordBag)
    
    // count how many times word appears in each document, build dictionary
    for word in wordSet do
        wordBag |> Seq.iter (updateDict dict word)
    
    // count how many documents each word appears in, remove those that appear only once
    for item in dict do
        if (countGlobalOccurrences item.Key dict) < 2 then
            wordSet.Remove(item.Key) |> ignore

    // now build matrix
    let fillMatrix (matrix : SparseMatrix) =
        let rec loop row =
            if row = matrix.RowCount then matrix
            else
                let key = wordSet.ElementAt(row)
                matrix.SetRow(row, dict.[key].ToArray())
                loop (row + 1)
        loop 0
    
    let countMatrix = fillMatrix (new SparseMatrix(wordSet.Count, docList.Length))
    (wordSet, countMatrix)
        
/// TFIDF: TFIDF (i,j) = ( Ni,j / N*,j ) * log( D / Di )
let TFIDF (matrix : SparseMatrix) = 
    let colCount = Matrix.foldByCol (fun acc elem -> acc + elem) 0.0 matrix
    let countNonZeroCols i = 
        Matrix.foldRow (fun acc elem -> if elem > 0.0 then (acc + 1) else acc) 0 matrix i
    let newCellVal i j =
        (matrix.[i,j] / colCount.[j]) * Math.Log(float matrix.ColumnCount / float (countNonZeroCols i))
    Matrix.inplaceAssign (fun i j -> newCellVal i j) matrix

[<EntryPoint>]
let main _ = 
    //let f = new Form()
    //Application.Run(f)
    
    let words, matrix =  buildMatrix docList
    printfn "These are the words that appear in two or more titles: "
    Seq.iter (fun elem -> printfn "%s" elem) words 
    
    printfn "\nThis is the count matrix: "
    printfn "%A" matrix
    
    
    //printfn "After TFIDF"
    //TFIDF matrix      -- uncomment to do SVD on TFIDF matrix
    //printfn "%A" matrix

    let svd = matrix.Svd(true)      // compute SVD
    
    printfn "\nThese are the singular values: "
    Seq.iter (fun e -> printfn "%A" e) (svd.S())
    

    //printfn "%A" (svd.U())    -- uncomment to print U
    //printfn "%A" (svd.VT())   -- uncomment to print VT


    printf "\nPress any key to exit: "
    Console.ReadKey(true) |> ignore

    0 // return an integer exit code

	